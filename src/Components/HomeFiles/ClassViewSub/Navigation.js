import React, { Component } from 'react';
import {NavLink,Link} from 'react-router-dom';

const Navigation=()=>{
  return(
    <div>
      <Link to="/class/address">Address</Link>
      <Link to="/class/homeTown">HomeTown</Link>
    </div>
  )
}
export default Navigation;
